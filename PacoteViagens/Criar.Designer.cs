﻿namespace PacoteViagens
{
    partial class Criar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(99, 118);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.Size = new System.Drawing.Size(204, 20);
            this.TextBoxPreco.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Preço Pack :";
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(99, 3);
            this.TextBoxNome.Multiline = true;
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.Size = new System.Drawing.Size(204, 99);
            this.TextBoxNome.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Descriçao Pack :";
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.BackgroundImage = global::PacoteViagens.Properties.Resources.save;
            this.ButtonGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonGravar.Location = new System.Drawing.Point(99, 144);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(75, 59);
            this.ButtonGravar.TabIndex = 19;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::PacoteViagens.Properties.Resources.exit;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Location = new System.Drawing.Point(228, 144);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 59);
            this.button2.TabIndex = 20;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Criar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PacoteViagens.Properties.Resources.create;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(307, 206);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ButtonGravar);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBoxNome);
            this.Controls.Add(this.label2);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Criar";
            this.Text = "Criar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button button2;
    }
}