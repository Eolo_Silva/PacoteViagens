﻿namespace PacoteViagens.Modelos
{
    public class Pacote
    {

        //#region atributos
        //private int _id;
        //private string _descricao;
        //private double _preco;

        //#endregion

        #region propriedades
        public int Id { get; set; }
        public string Descricao { get; set; }
        public double Preco { get; set; }
        #endregion


        #region Método Genéricos
        public override string ToString()
        {
            return string.Format("Id Pack:{0} - Pack:{1} - Preço Pack:{2}", Id, Descricao, Preco);
        }
        public override int GetHashCode()
        {
            return Id;
        }
        #endregion

    }
}
