﻿namespace PacoteViagens
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.ButtonLer = new System.Windows.Forms.Button();
            this.ButtonAtualizar = new System.Windows.Forms.Button();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ConfiguracaoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ficheiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gravarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.gravarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonSair
            // 
            this.ButtonSair.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = global::PacoteViagens.Properties.Resources.exit;
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonSair.Location = new System.Drawing.Point(437, 27);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(35, 32);
            this.ButtonSair.TabIndex = 0;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCriar.BackgroundImage = global::PacoteViagens.Properties.Resources.create;
            this.ButtonCriar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCriar.Location = new System.Drawing.Point(142, 221);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(52, 51);
            this.ButtonCriar.TabIndex = 1;
            this.ButtonCriar.UseVisualStyleBackColor = false;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // ButtonLer
            // 
            this.ButtonLer.BackColor = System.Drawing.Color.Transparent;
            this.ButtonLer.BackgroundImage = global::PacoteViagens.Properties.Resources.read;
            this.ButtonLer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonLer.Location = new System.Drawing.Point(200, 221);
            this.ButtonLer.Name = "ButtonLer";
            this.ButtonLer.Size = new System.Drawing.Size(52, 51);
            this.ButtonLer.TabIndex = 2;
            this.ButtonLer.UseVisualStyleBackColor = false;
            this.ButtonLer.Click += new System.EventHandler(this.ButtonLer_Click);
            // 
            // ButtonAtualizar
            // 
            this.ButtonAtualizar.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAtualizar.BackgroundImage = global::PacoteViagens.Properties.Resources.update;
            this.ButtonAtualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonAtualizar.Location = new System.Drawing.Point(261, 221);
            this.ButtonAtualizar.Name = "ButtonAtualizar";
            this.ButtonAtualizar.Size = new System.Drawing.Size(52, 51);
            this.ButtonAtualizar.TabIndex = 3;
            this.ButtonAtualizar.UseVisualStyleBackColor = false;
            this.ButtonAtualizar.Click += new System.EventHandler(this.ButtonAtualizar_Click);
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.BackColor = System.Drawing.Color.Transparent;
            this.ButtonApagar.BackgroundImage = global::PacoteViagens.Properties.Resources.delete;
            this.ButtonApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonApagar.Location = new System.Drawing.Point(321, 221);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(52, 51);
            this.ButtonApagar.TabIndex = 4;
            this.ButtonApagar.UseVisualStyleBackColor = false;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ConfiguracaoToolStripMenuItem,
            this.aboutToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(472, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ConfiguracaoToolStripMenuItem
            // 
            this.ConfiguracaoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ficheiroToolStripMenuItem});
            this.ConfiguracaoToolStripMenuItem.Name = "ConfiguracaoToolStripMenuItem";
            this.ConfiguracaoToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.ConfiguracaoToolStripMenuItem.Text = "Configurações";
            // 
            // ficheiroToolStripMenuItem
            // 
            this.ficheiroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lerToolStripMenuItem,
            this.gravarToolStripMenuItem,
            this.gravarToolStripMenuItem1});
            this.ficheiroToolStripMenuItem.Name = "ficheiroToolStripMenuItem";
            this.ficheiroToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ficheiroToolStripMenuItem.Text = "Ficheiro";
            // 
            // gravarComoToolStripMenuItem
            // 
            this.gravarToolStripMenuItem.Name = "gravarToolStripMenuItem";
            this.gravarToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.gravarToolStripMenuItem.Text = "Gravar Como...";
            this.gravarToolStripMenuItem.Click += new System.EventHandler(this.gravarComoToolStripMenuItem_Click);
            // 
            // lerToolStripMenuItem
            // 
            this.lerToolStripMenuItem.Name = "lerToolStripMenuItem";
            this.lerToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.lerToolStripMenuItem.Text = "Ler";
            this.lerToolStripMenuItem.Click += new System.EventHandler(this.lerToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // gravarToolStripMenuItem1
            // 
            this.gravarToolStripMenuItem1.Name = "gravarToolStripMenuItem1";
            this.gravarToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.gravarToolStripMenuItem1.Text = "Gravar";
            this.gravarToolStripMenuItem1.Click += new System.EventHandler(this.gravarToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PacoteViagens.Properties.Resources.backGround;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(472, 284);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonApagar);
            this.Controls.Add(this.ButtonAtualizar);
            this.Controls.Add(this.ButtonLer);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Pacotes de Viagens";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonLer;
        private System.Windows.Forms.Button ButtonAtualizar;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ConfiguracaoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ficheiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gravarToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem lerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gravarToolStripMenuItem1;
    }
}

