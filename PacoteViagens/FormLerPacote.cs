﻿namespace PacoteViagens
{
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;
    public partial class FormLerPacote : Form
    {
        private List<Pacote> ListaDePacotes;
        public FormLerPacote()
        {

        }
        public FormLerPacote(List<Pacote> listaPacotes)
        {
            InitializeComponent();
            ControlBox = false;
            this.ListaDePacotes = listaPacotes;

            for(int i = 0; i < ListaDePacotes.Count; i++)
            {
                DataGridViewLer.Rows.Add(ListaDePacotes[i].Id, ListaDePacotes[i].Descricao, ListaDePacotes[i].Preco);
                DataGridViewLer.AllowUserToAddRows = false;
            }
        }

        private void ButtonSair_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void FormLerPacote_Load(object sender, System.EventArgs e)
        {

        }
    }
}
