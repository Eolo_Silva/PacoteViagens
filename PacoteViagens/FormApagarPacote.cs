﻿namespace PacoteViagens
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;
    public partial class FormApagarPacote : Form
    {
        private List<Pacote> ListaDePacotes;
        public FormApagarPacote()
        {
        }

        public FormApagarPacote(List<Pacote> listaPacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = listaPacotes;
            ControlBox = false;
            ComboBoxListaPacotes.DataSource = ListaDePacotes;
        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            if (ComboBoxListaPacotes.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione algum pacote de viagem");
            }
            else
            {
                ListaDePacotes.RemoveAt(ComboBoxListaPacotes.SelectedIndex);
                MessageBox.Show("Pacote de viagem Apagado com Sucesso");
                ComboBoxListaPacotes.DataSource = null;
                ComboBoxListaPacotes.DataSource = ListaDePacotes;
            }
        }
        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O pacote de viagem não foi apagado");
            Close();
        }
    }
}
