﻿namespace PacoteViagens
{
    using System;
    using System.Windows.Forms;
    using Modelos;
    using System.Collections.Generic;
    using System.IO;

    public partial class Form1 : Form
    {
        Criar c = new Criar();
        FormApagarPacote a = new FormApagarPacote();
        FormLerPacote l = new FormLerPacote();
        FormAtualizarPacote ap = new FormAtualizarPacote();
        Pacote PV = new Pacote();


        private List<Pacote> ListaDePacotes;

        public Form1()
        {
            InitializeComponent();
            ListaDePacotes = new List<Pacote>();
            ControlBox = false;

            //ListaDePacotes.Add(new Pacote { Id = 1, Descricao = "Amazonas", Preco = 1800.5 });
            //ListaDePacotes.Add(new Pacote { Id = 2, Descricao = "NewYork", Preco = 987.3 });
            //ListaDePacotes.Add(new Pacote { Id = 3, Descricao = "Paris", Preco = 232.4 });
            //ListaDePacotes.Add(new Pacote { Id = 4, Descricao = "Douro", Preco = 187.5 });
            //ListaDePacotes.Add(new Pacote { Id = 5, Descricao = "Piramides", Preco = 567.2 });
            //ListaDePacotes.Add(new Pacote { Id = 6, Descricao = "Pirineus", Preco = 378.5 });
            //ListaDePacotes.Add(new Pacote { Id = 7, Descricao = "Algarve", Preco = 220 });

        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            c = new Criar(ListaDePacotes);
            c.Show();
        }

        private void ButtonLer_Click(object sender, EventArgs e)
        {
            l = new FormLerPacote(ListaDePacotes);
            l.Show();
        }

        private void ButtonAtualizar_Click(object sender, EventArgs e)
        {
            ap = new FormAtualizarPacote(ListaDePacotes);
            ap.Show();
        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            a = new FormApagarPacote(ListaDePacotes);
            a.Show();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Formando:\nEolo Guerra Silva\nCET30 n.6\nPacote de Viagens Versão 1.0.0\n06-10-2017");
        }

        private void gravarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text file|*.txt";
            saveFileDialog1.Title = "Salvar Como Ficheiro de Texto";
            saveFileDialog1.ShowDialog();

            // Se o nome do arquivo não for uma seqüência vazia, abra-o para salvar.
            if (saveFileDialog1.FileName != "")
            {
                // Salva a imagem através de um FileStream criado pelo método OpenFile.
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();

                StreamWriter sw = new StreamWriter(fs);//se nao existir, grava

                //verficar o conteudo da lista de pacotes de viagem
                foreach (var pacote in ListaDePacotes)
                {
                    string linha = string.Format("{0};{1};{2}", pacote.Id, pacote.Descricao, pacote.Preco);
                    sw.WriteLine(linha);

                }
                sw.Close();
                fs.Close();

                localizarFicheiro = saveFileDialog1.FileName;
            }
        }

        private void lerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader myStream;
            OpenFileDialog OpenFileDialog1 = new OpenFileDialog();
           
            OpenFileDialog1.Filter = "Text file|*.txt";
            OpenFileDialog1.RestoreDirectory = true;

            if (OpenFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = new StreamReader(OpenFileDialog1.OpenFile())) != null)
                    {
                        using (myStream)
                        {
                            ListaDePacotes.Clear();
                            string linha = "";

                            while ((linha = myStream.ReadLine()) != null)
                            {
                                string[] campos = new string[3];
                                campos = linha.Split(';');

                                var pacote = new Pacote
                                {
                                    Id = Convert.ToInt32(campos[0]),
                                    Descricao = campos[1],
                                    Preco = Convert.ToDouble(campos[2])
                                };

                                ListaDePacotes.Add(pacote);
                            }

                            myStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: Não foi possível ler o arquivo do disco. Erro original: " + ex.Message);
                }
            }
        }

        private string localizarFicheiro = null;


        private void gravarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (localizarFicheiro != null)
            {
                StreamWriter sw = new StreamWriter(localizarFicheiro);
                try
                {
                    if (!File.Exists(localizarFicheiro))
                    {
                        sw = File.CreateText(localizarFicheiro);
                    }

                    foreach (var pacote in ListaDePacotes)
                    {
                        string linha = string.Format("{0};{1};{2}", pacote.Id, pacote.Descricao, pacote.Preco);
                        sw.WriteLine(linha);
                    }

                    sw.Close();
                }
                catch (Exception ev)
                {
                    MessageBox.Show(ev.Message);
                }
            }
            else
            {
                MessageBox.Show("Tem que gravar como.. primeiro");
            }  
        }
    }
}
