﻿namespace PacoteViagens
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormAtualizarPacote : Form
    {
        private List<Pacote> ListaDePacotes;
        public FormAtualizarPacote(List<Pacote> ListaDePacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = ListaDePacotes;
            TextBoxDescricao.Text = ListaDePacotes[0].Descricao;
            TextBoxPreco.Text = ListaDePacotes[0].Preco.ToString();
            ControlBox = false;
        }

        public FormAtualizarPacote()
        {
        }

        private int num = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (num == 0)
            {
                TextBoxDescricao.Text = ListaDePacotes[0].Descricao;
                TextBoxPreco.Text = ListaDePacotes[0].Preco.ToString();
               
            }
            else
            {
                TextBoxDescricao.Text = ListaDePacotes[num].Descricao;
                TextBoxPreco.Text = ListaDePacotes[num].Preco.ToString();
                num -= 1;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (num == ListaDePacotes.Count - 1)
            {
                TextBoxDescricao.Text = ListaDePacotes[num].Descricao;
                TextBoxPreco.Text = ListaDePacotes[num].Preco.ToString();
                
            }
            else
            {
                TextBoxDescricao.Text = ListaDePacotes[num].Descricao;
                TextBoxPreco.Text = ListaDePacotes[num].Preco.ToString();
                num += 1;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                MessageBox.Show("Tem que inserir a descrição do pacote de viagem.");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxPreco.Text))
            {
                MessageBox.Show("Tem que inserir o preço do pacote de viagem.");
                return;
            }

            ListaDePacotes[num].Descricao = TextBoxDescricao.Text;
            ListaDePacotes[num].Preco = Convert.ToDouble(TextBoxPreco.Text);
            MessageBox.Show("Atualização feita com sucesso");
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormAtualizarPacote_Load(object sender, EventArgs e)
        {

        }
    }
}

