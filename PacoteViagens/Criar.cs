﻿namespace PacoteViagens
{
    using System;
    using System.Windows.Forms;
    using Modelos;
    using System.Collections.Generic;

    public partial class Criar : Form
    {
        private List<Pacote> listaDeAlunos = new List<Pacote>();
        private List<Pacote> ListaDePacotes;

        public Criar(List<Pacote> listaPacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = listaPacotes;
            ControlBox = false;

        }

        public Criar()
        {
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxNome.Text))
            {
                MessageBox.Show("Tem que inserir a descrição do pacote de viagens.");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxPreco.Text))
            {
                MessageBox.Show("Tem que inserir o preço do pacote de viagens.");
                return;
            }

            var pacote = new Pacote
            {
                Id = GeraIdPacote(),               
                Descricao = TextBoxNome.Text,
                Preco = Convert.ToDouble(TextBoxPreco.Text)
            };

            ListaDePacotes.Add(pacote);
            MessageBox.Show("Novo pacote de viagem inserido com sucesso");
            Close();
        }
        private int GeraIdPacote()
        {
            return ListaDePacotes[ListaDePacotes.Count - 1].Id + 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O pacote de viagem não foi criado.");
            Close();
        }
    }
}
