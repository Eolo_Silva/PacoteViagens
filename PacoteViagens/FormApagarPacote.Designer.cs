﻿namespace PacoteViagens
{
    partial class FormApagarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ComboBoxListaPacotes = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.BackgroundImage = global::PacoteViagens.Properties.Resources.delete;
            this.ButtonApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonApagar.Location = new System.Drawing.Point(37, 59);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(75, 62);
            this.ButtonApagar.TabIndex = 0;
            this.ButtonApagar.UseVisualStyleBackColor = true;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.BackgroundImage = global::PacoteViagens.Properties.Resources.exit;
            this.ButtonCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCancelar.Location = new System.Drawing.Point(231, 87);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(41, 34);
            this.ButtonCancelar.TabIndex = 1;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ComboBoxListaPacotes
            // 
            this.ComboBoxListaPacotes.FormattingEnabled = true;
            this.ComboBoxListaPacotes.Location = new System.Drawing.Point(13, 13);
            this.ComboBoxListaPacotes.Name = "ComboBoxListaPacotes";
            this.ComboBoxListaPacotes.Size = new System.Drawing.Size(259, 21);
            this.ComboBoxListaPacotes.TabIndex = 2;
            // 
            // FormApagarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 133);
            this.Controls.Add(this.ComboBoxListaPacotes);
            this.Controls.Add(this.ButtonCancelar);
            this.Controls.Add(this.ButtonApagar);
            this.Name = "FormApagarPacote";
            this.ShowIcon = false;
            this.Text = "FormApagarPacote";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.ComboBox ComboBoxListaPacotes;
    }
}